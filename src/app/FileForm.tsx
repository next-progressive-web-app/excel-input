"use client";

import { useState } from "react";
import { read, utils } from "xlsx";

export default function FileForm() {
  const [files, setFiles]: [any, Function] = useState();

  function parseWorkBook(file: File) {
    let reader = new FileReader();
    reader.readAsBinaryString(file);

    reader.onload = (evt) => {
        let data = evt.target?.result;
        let workbook = read(data, {type: "binary"});

        let result = {};

        workbook.SheetNames.forEach(sheetName => {
            let json = utils.sheet_to_json(workbook.Sheets[sheetName]);
            console.log(json);
        })
    }
  }
  function formSubmitListener(evt: any) {
    evt.preventDefault();

    if(files === undefined || files.length === 0) {
        alert("Please choose a file");
        return;
    }

    for(let i = 0; i < files.length; i++) {
        let file = files[i];
        let filename = file.name;
        let extension = filename.substring(filename.lastIndexOf(".")).toUpperCase();
        if(extension === ".XLS" || extension === ".XLSX") {
            console.log(`file: ${filename} was accepted`);
            parseWorkBook(file);
        }   else    {
            console.log(`file: ${filename} was ignored as it is not an XSLX file`);
        }
    }
  }

  return (
    <div>
      <form
        onSubmit={formSubmitListener}
        className="flex flex-col align-center justify-center"
      >
        <label
          className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
          htmlFor="fileInput"
        >
            Select Excel Files
        </label>
        <input
          className="block w-full mb-5 text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400"
          id="fileInput"
          type="file"
          multiple
          onChange = {(evt) => { setFiles(evt.target.files) }}
        />
        <button
          type="submit"
          className="text-white bg-blue-700 hover:bg-blue-800 focus:bg-blue-800 font-medium rounded-md text-sm px-5 py-2.5 mt-2"
        >
          Submit Files
        </button>
      </form>
    </div>
  );
}
