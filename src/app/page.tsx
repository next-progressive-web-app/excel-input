import Image from "next/image";
import FileForm from "./FileForm";

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between py-24 px-2">
      <FileForm />
    </main>
  );
}
